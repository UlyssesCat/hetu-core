package io.hetu.core.plugin.dm;

public class DMClientModule implements Module {
    @Override
    public void configure(Binder binder) {
        binder.bind(JdbcClient.class).to(DMClient.class).in(Scopes.SINGLETON);
        configBinder(binder).bindConfig(BaseJdbcConfig.class);
        configBinder(binder).bindConfig(DMConfig.class);
    }
    @Provides
    @Singleton
    public static ConnectionFactory getConnectionFactory(BaseJdbcConfig config, DMConfig dmConfig)
    {
        Properties connectionProperties = basicConnectionProperties(config);

        Driver driver;
        try {
            driver = (Driver) Class.forName(Constants.DM_JDBC_DRIVER_CLASS_NAME).getConstructor(((Class<?>[]) null)).newInstance();
        }
        catch (InstantiationException | ClassNotFoundException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new PrestoException(JDBC_ERROR, e);
        }

        return new DriverConnectionFactory(driver, config.getConnectionUrl(),
                Optional.ofNullable(config.getUserCredentialName()),
                Optional.ofNullable(config.getPasswordCredentialName()), connectionProperties);
    }
}