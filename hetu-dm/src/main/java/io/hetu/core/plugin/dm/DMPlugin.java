package io.hetu.core.plugin.dm;

@ConnectorConfig(connectorLabel = "DM : Query and create tables on an external DM database",
        propertiesEnabled = true)
public class DMPlugin extends JdbcPlugin {
    public DMPlugin() {
        super("dm", new DMClientModule());
    }

    @Override
    public Optional<ConnectorWithProperties> getConnectorWithProperties()
    {
        ConnectorConfig connectorConfig = DMPlugin.class.getAnnotation(ConnectorConfig.class);
        Optional<ConnectorWithProperties> connectorWithProperties = ConnectorUtil.assembleConnectorProperties(connectorConfig,
                Arrays.asList(BaseJdbcConfig.class.getDeclaredMethods()));
        ConnectorUtil.addConnUrlProperty(connectorWithProperties, "jdbc:dm://host:port");
        return connectorWithProperties;
    }
}